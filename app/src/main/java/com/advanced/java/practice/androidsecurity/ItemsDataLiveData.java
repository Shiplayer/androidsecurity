package com.advanced.java.practice.androidsecurity;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

/**
 * Created by Anton on 25.02.2018.
 */

public class ItemsDataLiveData extends LiveData<ItemsData[]> {

    @Override
    protected void onActive() {
        super.onActive();
        getAllItemsData();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    public void getAllItemsData() {
        AsyncTask.execute(() ->{
            ItemsData[] itemsData = new ItemsData[50];
            for(int i = 0; i < itemsData.length; i++){
                itemsData[i] = new ItemsData(false, "name" + i, "description" + i);
            }
            postValue(itemsData);
        });
    }

    public void change(ItemsData[] itemsData){
        postValue(itemsData);
    }
}
