package com.advanced.java.practice.androidsecurity.fragments.second;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.advanced.java.practice.androidsecurity.ItemsData;
import com.advanced.java.practice.androidsecurity.ItemsDataViewModel;
import com.advanced.java.practice.androidsecurity.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {
    private ItemsDataViewModel viewModel;
    private SecondRecyclerViewAdapter mAdapter;


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getActivity()).get(ItemsDataViewModel.class);
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.rv_card_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new SecondRecyclerViewAdapter();
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getLiveData().observe(getActivity(), (itemsData ->{
            List<ItemsData> itemsDataList = new ArrayList<>();
            for(ItemsData item : itemsData){
                if(item.isSelected())
                    itemsDataList.add(item);
            }
            mAdapter.setData(itemsDataList.toArray(new ItemsData[itemsDataList.size()]));
        }));
    }
}
