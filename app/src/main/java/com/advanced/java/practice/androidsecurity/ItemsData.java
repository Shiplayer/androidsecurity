package com.advanced.java.practice.androidsecurity;

/**
 * Created by Anton on 25.02.2018.
 */

public class ItemsData {
    private boolean mSelected;
    private String mName;
    private String mDescription;

    public ItemsData(boolean mSelected, String mName, String mDescription) {
        this.mSelected = mSelected;
        this.mName = mName;
        this.mDescription = mDescription;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean mSelected) {
        this.mSelected = mSelected;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    @Override
    public String toString() {
        return "ItemsData{" +
                "mSelected=" + mSelected +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                '}';
    }
}
