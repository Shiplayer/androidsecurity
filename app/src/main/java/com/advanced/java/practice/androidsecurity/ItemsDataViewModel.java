package com.advanced.java.practice.androidsecurity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by Anton on 25.02.2018.
 */

public class ItemsDataViewModel extends ViewModel {
    private ItemsDataLiveData data = new ItemsDataLiveData();

    public void setDataChange(ItemsData[] dataChange){
        data.change(dataChange);
    }

    public LiveData<ItemsData[]> getLiveData(){
        return data;
    }
}
