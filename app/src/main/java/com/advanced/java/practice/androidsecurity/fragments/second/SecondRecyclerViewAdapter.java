package com.advanced.java.practice.androidsecurity.fragments.second;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.advanced.java.practice.androidsecurity.ItemsData;
import com.advanced.java.practice.androidsecurity.R;

import java.util.zip.Inflater;

/**
 * Created by Anton on 25.02.2018.
 */

public class SecondRecyclerViewAdapter extends RecyclerView.Adapter<SecondRecyclerViewAdapter.ViewHolder> {
    private ItemsData[] itemsData;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.second_cv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(itemsData[position].getName());
        holder.tvDescription.setText(itemsData[position].getDescription());
    }

    @Override
    public int getItemCount() {
        return itemsData == null ? 0 : itemsData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.cv_name);
            tvDescription = itemView.findViewById(R.id.cv_description);
        }
    }

    public void setData(ItemsData[] data){
        itemsData = data;
        notifyDataSetChanged();
    }
}
