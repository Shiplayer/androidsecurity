package com.advanced.java.practice.androidsecurity.fragments.first;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.advanced.java.practice.androidsecurity.ItemsData;
import com.advanced.java.practice.androidsecurity.R;

/**
 * Created by Anton on 25.02.2018.
 */

public class FirstRecyclerViewAdapter extends RecyclerView.Adapter<FirstRecyclerViewAdapter.ViewHolder> {
    private ItemsData[] mItemsData;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.first_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(mItemsData[position].getName());
        holder.cbSelected.setText("clicked!");
        holder.cbSelected.setChecked(mItemsData[position].isSelected());
        holder.cbSelected.setOnClickListener(view -> mItemsData[holder.getAdapterPosition()].setSelected(holder.cbSelected.isChecked()));
    }

    @Override
    public int getItemCount() {
        return mItemsData == null ? 0 : mItemsData.length;
    }

    public void setData(ItemsData[] data){
        Log.w(FirstRecyclerViewAdapter.class.getSimpleName(), "length = " + data.length);
        mItemsData = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CheckBox cbSelected;
        public TextView tvName;
        public ViewHolder(View itemView) {
            super(itemView);
            cbSelected = itemView.findViewById(R.id.cb_selected);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }
}
