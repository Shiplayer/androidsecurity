package com.advanced.java.practice.androidsecurity.fragments.first;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.advanced.java.practice.androidsecurity.ItemsDataViewModel;
import com.advanced.java.practice.androidsecurity.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {
    ItemsDataViewModel mViewModel;
    FirstRecyclerViewAdapter mAdapter;


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.rv_first_fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FirstRecyclerViewAdapter();
        recyclerView.setAdapter(mAdapter);
        mViewModel = ViewModelProviders.of(getActivity()).get(ItemsDataViewModel.class);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.getLiveData().observe(getActivity(), mAdapter::setData);
    }
}
